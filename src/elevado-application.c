/* elevado-application.c
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#include "config.h"

#include "elevado-application.h"
#include "elevado-window.h"

#include <atspi/atspi.h>

struct _ElevadoApplication
{
  AdwApplication parent_instance;
};

G_DEFINE_FINAL_TYPE (ElevadoApplication, elevado_application, ADW_TYPE_APPLICATION)


/*
 * Callbacks
 */

static void
elevado_application_quit_action (GSimpleAction *action,
                                 GVariant      *parameter,
                                 gpointer       user_data)
{
  ElevadoApplication *self = user_data;

  g_assert (ELEVADO_IS_APPLICATION (self));

  g_application_quit (G_APPLICATION (self));
}


/*
 * GApplication overrides
 */

static void
elevado_application_startup (GApplication *app)
{
  atspi_init ();

  G_APPLICATION_CLASS (elevado_application_parent_class)->startup (app);
}

static void
elevado_application_activate (GApplication *app)
{
  GtkWindow *window;

  g_assert (ELEVADO_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));

  if (window == NULL)
    window = g_object_new (ELEVADO_TYPE_WINDOW,
                           "application", app,
                           NULL);

  gtk_window_present (window);
}

static void
elevado_application_class_init (ElevadoApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->startup = elevado_application_startup;
  app_class->activate = elevado_application_activate;
}

static void
elevado_application_init (ElevadoApplication *self)
{
  static const GActionEntry app_actions[] = {
    { "quit", elevado_application_quit_action },
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   app_actions,
                                   G_N_ELEMENTS (app_actions),
                                   self);
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const char *[]) { "<primary>q", NULL });

}

ElevadoApplication *
elevado_application_new (const char        *application_id,
                         GApplicationFlags  flags)
{
  g_return_val_if_fail (application_id != NULL, NULL);

  return g_object_new (ELEVADO_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       "resource-base-path", "/com/feaneron/Elevado",
                       NULL);
}

