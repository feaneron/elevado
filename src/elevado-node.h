/*
 * elevado-node.h
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#pragma once

#include <glib-object.h>
#include <atspi/atspi.h>

G_BEGIN_DECLS

#define ELEVADO_TYPE_NODE (elevado_node_get_type())
G_DECLARE_FINAL_TYPE (ElevadoNode, elevado_node, ELEVADO, NODE, GObject)

ElevadoNode * elevado_node_new (AtspiAccessible *accessible);

AtspiAccessible * elevado_node_get_accessible (ElevadoNode *self);

const char * elevado_node_get_name (ElevadoNode *self);
const char * elevado_node_get_role_icon_name (ElevadoNode *self);
const char * elevado_node_get_role_name (ElevadoNode *self);

G_END_DECLS
