/*
 * elevado-node-details.h
 *
 * Copyright 2024 Red Hat.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Bilal Elmoussaoui <belmouss@redhat.com>
 */

#pragma once

#include <adwaita.h>

#include "elevado-node.h"

G_BEGIN_DECLS

#define ELEVADO_TYPE_NODE_DETAILS (elevado_node_details_get_type())
G_DECLARE_FINAL_TYPE (ElevadoNodeDetails, elevado_node_details, ELEVADO, NODE_DETAILS, AdwBin)

void elevado_node_details_set_node (ElevadoNodeDetails *self,
                                    ElevadoNode        *node);

G_END_DECLS
