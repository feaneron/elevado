/*
 * elevado-node.c
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#include "elevado-node.h"

#include "elevado-autoptr.h"

#include <gio/gio.h>

struct _ElevadoNode
{
  GObject parent_instance;

  GPtrArray *children;
  int n_children;

  AtspiAccessible *accessible;
  AtspiRole role;
  char *role_name;
  char *name;
};

static void g_list_model_interface_init (GListModelInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (ElevadoNode, elevado_node, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                      g_list_model_interface_init))

enum {
  PROP_0,
  PROP_ACCESSIBLE,
  PROP_NAME,
  PROP_N_CHILDREN,
  PROP_OBJECT_PATH,
  PROP_ROLE_ICON_NAME,
  PROP_ROLE_NAME,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];


/*
 * Auxiliary function
 */

static const char *
icon_name_from_role (AtspiRole role)
{
  switch (role)
    {
    case ATSPI_ROLE_APPLICATION:
      return "application-x-executable-symbolic";

    case ATSPI_ROLE_ACCELERATOR_LABEL:
      return "tag-outline-symbolic";

    case ATSPI_ROLE_ALERT:
      return "preferences-system-notifications-symbolic";

    case ATSPI_ROLE_ARROW:
      return "code-symbolic";

    case ATSPI_ROLE_CALENDAR:
      return "x-office-calendar-symbolic";

    case ATSPI_ROLE_DIALOG:
      return "dialog-symbolic";

    case ATSPI_ROLE_PAGE_TAB:
    case ATSPI_ROLE_PAGE_TAB_LIST:
      return "tab-symbolic";

    case ATSPI_ROLE_TABLE:
    case ATSPI_ROLE_TABLE_CELL:
    case ATSPI_ROLE_TABLE_COLUMN_HEADER:
    case ATSPI_ROLE_TABLE_ROW_HEADER:
    case ATSPI_ROLE_TREE:
    case ATSPI_ROLE_TREE_TABLE:
      return "table-symbolic";

    case ATSPI_ROLE_WINDOW:
      return "window-symbolic";

    case ATSPI_ROLE_MENU:
    case ATSPI_ROLE_MENU_BAR:
    case ATSPI_ROLE_MENU_ITEM:
    case ATSPI_ROLE_INFO_BAR:
      return "banner-symbolic";

    case ATSPI_ROLE_FRAME:
    case ATSPI_ROLE_DESKTOP_FRAME:
    case ATSPI_ROLE_INTERNAL_FRAME:
      return "window-flat-symbolic";

    case ATSPI_ROLE_CHECK_MENU_ITEM:
    case ATSPI_ROLE_CHECK_BOX:
      return "checkbox-checked-symbolic";

    case ATSPI_ROLE_RADIO_BUTTON:
    case ATSPI_ROLE_RADIO_MENU_ITEM:
      return "radio-checked-symbolic";

    case ATSPI_ROLE_SEPARATOR:
      return "view-continuous-symbolic";

    case ATSPI_ROLE_SPLIT_PANE:
    case ATSPI_ROLE_PANEL:
      return "panel-left-symbolic";

    case ATSPI_ROLE_SCROLL_BAR:
    case ATSPI_ROLE_SCROLL_PANE:
      return "window-scrolling-symbolic";

    case ATSPI_ROLE_FILLER:
      return "selection-opaque-2-symbolic";

    case ATSPI_ROLE_LIST:
    case ATSPI_ROLE_LIST_ITEM:
      return "list-symbolic";

    case ATSPI_ROLE_DOCUMENT_SPREADSHEET:
      return "x-office-spreadsheet-symbolic";

    case ATSPI_ROLE_DOCUMENT_PRESENTATION:
      return "x-office-presentation-symbolic";

    case ATSPI_ROLE_DOCUMENT_TEXT:
    case ATSPI_ROLE_TEXT:
    case ATSPI_ROLE_FONT_CHOOSER:
      return "font-x-generic-symbolic";

    case ATSPI_ROLE_DOCUMENT_EMAIL:
      return "mail-unread-symbolic";

    case ATSPI_ROLE_DOCUMENT_WEB:
      return "web-browser-symbolic";

    case ATSPI_ROLE_LINK:
      return "chain-link-symbolic";

    case ATSPI_ROLE_ICON:
    case ATSPI_ROLE_IMAGE:
      return "image-x-generic-symbolic";

    case ATSPI_ROLE_COLOR_CHOOSER:
      return "color-picker-symbolic";

    case ATSPI_ROLE_DIAL:
      return "dialpad-symbolic";

    case ATSPI_ROLE_FILE_CHOOSER:
      return "file-manager-symbolic";

    case ATSPI_ROLE_PASSWORD_TEXT:
      return "password-entry-symbolic";

    case ATSPI_ROLE_AUDIO:
      return "music-note-symbolic";

    case ATSPI_ROLE_VIDEO:
      return "video-clip-symbolic";

    case ATSPI_ROLE_TERMINAL:
      return "terminal-symbolic";

    case ATSPI_ROLE_TIMER:
      return "timer-symbolic";

    case ATSPI_ROLE_INVALID:
    case ATSPI_ROLE_ANIMATION:
    case ATSPI_ROLE_CANVAS:
    case ATSPI_ROLE_COLUMN_HEADER:
    case ATSPI_ROLE_COMBO_BOX:
    case ATSPI_ROLE_DATE_EDITOR:
    case ATSPI_ROLE_DESKTOP_ICON:
    case ATSPI_ROLE_DIRECTORY_PANE:
    case ATSPI_ROLE_DRAWING_AREA:
    case ATSPI_ROLE_FOCUS_TRAVERSABLE:
    case ATSPI_ROLE_GLASS_PANE:
    case ATSPI_ROLE_HTML_CONTAINER:
    case ATSPI_ROLE_LABEL:
    case ATSPI_ROLE_LAYERED_PANE:
    case ATSPI_ROLE_OPTION_PANE:
    case ATSPI_ROLE_POPUP_MENU:
    case ATSPI_ROLE_PROGRESS_BAR:
    case ATSPI_ROLE_PUSH_BUTTON:
    case ATSPI_ROLE_ROOT_PANE:
    case ATSPI_ROLE_ROW_HEADER:
    case ATSPI_ROLE_SLIDER:
    case ATSPI_ROLE_SPIN_BUTTON:
    case ATSPI_ROLE_STATUS_BAR:
    case ATSPI_ROLE_TEAROFF_MENU_ITEM:
    case ATSPI_ROLE_TOGGLE_BUTTON:
    case ATSPI_ROLE_TOOL_BAR:
    case ATSPI_ROLE_TOOL_TIP:
    case ATSPI_ROLE_UNKNOWN:
    case ATSPI_ROLE_VIEWPORT:
    case ATSPI_ROLE_EXTENDED:
    case ATSPI_ROLE_HEADER:
    case ATSPI_ROLE_FOOTER:
    case ATSPI_ROLE_PARAGRAPH:
    case ATSPI_ROLE_RULER:
    case ATSPI_ROLE_AUTOCOMPLETE:
    case ATSPI_ROLE_EDITBAR:
    case ATSPI_ROLE_EMBEDDED:
    case ATSPI_ROLE_ENTRY:
    case ATSPI_ROLE_CHART:
    case ATSPI_ROLE_CAPTION:
    case ATSPI_ROLE_DOCUMENT_FRAME:
    case ATSPI_ROLE_HEADING:
    case ATSPI_ROLE_PAGE:
    case ATSPI_ROLE_SECTION:
    case ATSPI_ROLE_REDUNDANT_OBJECT:
    case ATSPI_ROLE_FORM:
    case ATSPI_ROLE_INPUT_METHOD_WINDOW:
    case ATSPI_ROLE_TABLE_ROW:
    case ATSPI_ROLE_TREE_ITEM:
    case ATSPI_ROLE_COMMENT:
    case ATSPI_ROLE_LIST_BOX:
    case ATSPI_ROLE_GROUPING:
    case ATSPI_ROLE_IMAGE_MAP:
    case ATSPI_ROLE_NOTIFICATION:
    case ATSPI_ROLE_LEVEL_BAR:
    case ATSPI_ROLE_TITLE_BAR:
    case ATSPI_ROLE_BLOCK_QUOTE:
    case ATSPI_ROLE_DEFINITION:
    case ATSPI_ROLE_ARTICLE:
    case ATSPI_ROLE_LANDMARK:
    case ATSPI_ROLE_LOG:
    case ATSPI_ROLE_MARQUEE:
    case ATSPI_ROLE_MATH:
    case ATSPI_ROLE_RATING:
    case ATSPI_ROLE_STATIC:
    case ATSPI_ROLE_MATH_FRACTION:
    case ATSPI_ROLE_MATH_ROOT:
    case ATSPI_ROLE_SUBSCRIPT:
    case ATSPI_ROLE_SUPERSCRIPT:
    case ATSPI_ROLE_DESCRIPTION_LIST:
    case ATSPI_ROLE_DESCRIPTION_TERM:
    case ATSPI_ROLE_DESCRIPTION_VALUE:
    case ATSPI_ROLE_FOOTNOTE:
    case ATSPI_ROLE_CONTENT_DELETION:
    case ATSPI_ROLE_CONTENT_INSERTION:
    case ATSPI_ROLE_MARK:
    case ATSPI_ROLE_SUGGESTION:
    case ATSPI_ROLE_PUSH_BUTTON_MENU:
    case ATSPI_ROLE_LAST_DEFINED:
    default:
      return "dialog-question-symbolic";
    }

  return "dialog-question-symbolic";
}

static void
unref_object_nullsafe (gpointer data)
{
  g_clear_object (&data);
}

static void
pull_properties_from_accessible (ElevadoNode *self,
                                 gboolean     notify)
{
  if (notify)
    {
      int old_n_children = self->n_children;

      g_ptr_array_set_size (self->children, 0);
      self->n_children = 0;

      g_list_model_items_changed (G_LIST_MODEL (self), 0, old_n_children, 0);
    }

  g_clear_pointer (&self->name, g_free);
  self->name = atspi_accessible_get_name (self->accessible, NULL);

  g_clear_pointer (&self->role_name, g_free);
  self->role_name = atspi_accessible_get_role_name (self->accessible, NULL);

  self->role = atspi_accessible_get_role (self->accessible, NULL);
  self->n_children = atspi_accessible_get_child_count (self->accessible, NULL);

  for (int i = 0; i < self->n_children; i++)
    g_ptr_array_add (self->children, NULL);

  if (notify)
    {
      g_object_freeze_notify (G_OBJECT (self));
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NAME]);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_N_CHILDREN]);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ROLE_NAME]);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ROLE_ICON_NAME]);
      g_object_thaw_notify (G_OBJECT (self));
    }
}

/*
 * GListModel interface
 */

static GType
elevado_node_get_item_type (GListModel *model)
{
  return ELEVADO_TYPE_NODE;
}

static gpointer
elevado_node_get_item (GListModel   *model,
                       unsigned int  position)
{
  ElevadoNode *self = ELEVADO_NODE (model);
  ElevadoNode *node = NULL;

  if (position >= self->children->len)
    return NULL;

  node = g_ptr_array_index (self->children, position);
  if (!node)
    {
      g_autoptr(AtspiAccessible) child_accessible = NULL;

      child_accessible = atspi_accessible_get_child_at_index (self->accessible,
                                                              position,
                                                              NULL);
      g_assert (child_accessible != NULL);

      node = elevado_node_new (child_accessible);

      g_ptr_array_steal_index (self->children, position);
      g_ptr_array_insert (self->children, position, node);
    }

  return g_object_ref (node);
}

static unsigned int
elevado_node_get_n_items (GListModel *model)
{
  ElevadoNode *self = ELEVADO_NODE (model);

  return self->n_children;
}

static void
g_list_model_interface_init (GListModelInterface *iface)
{
  iface->get_item_type = elevado_node_get_item_type;
  iface->get_item = elevado_node_get_item;
  iface->get_n_items = elevado_node_get_n_items;
}


/*
 * GObject overrides
 */

static void
elevado_node_finalize (GObject *object)
{
  ElevadoNode *self = (ElevadoNode *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);
  g_clear_pointer (&self->role_name, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->accessible);

  G_OBJECT_CLASS (elevado_node_parent_class)->finalize (object);
}

static void
elevado_node_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ElevadoNode *self = ELEVADO_NODE (object);

  switch (prop_id)
    {
    case PROP_ACCESSIBLE:
      g_value_set_object (value, self->accessible);
      break;

    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_N_CHILDREN:
      g_value_set_uint (value, self->n_children);
      break;

    case PROP_OBJECT_PATH:
      g_value_set_string (value, ATSPI_OBJECT (self->accessible)->path);
      break;

    case PROP_ROLE_ICON_NAME:
      g_value_set_string (value, icon_name_from_role (self->role));
      break;

    case PROP_ROLE_NAME:
      g_value_set_string (value, self->role_name);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ElevadoNode *self = ELEVADO_NODE (object);

  switch (prop_id)
    {
    case PROP_ACCESSIBLE:
      g_assert (self->accessible == NULL);
      self->accessible = g_value_dup_object (value);
      g_assert (self->accessible != NULL);

      pull_properties_from_accessible (self, FALSE);

      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_class_init (ElevadoNodeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = elevado_node_finalize;
  object_class->get_property = elevado_node_get_property;
  object_class->set_property = elevado_node_set_property;

  properties[PROP_ACCESSIBLE] =
    g_param_spec_object ("accessible", NULL, NULL,
                         ATSPI_TYPE_ACCESSIBLE,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         "",
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties[PROP_N_CHILDREN] =
      g_param_spec_uint ("n-children", NULL, NULL,
                         0, G_MAXUINT32, 0,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties[PROP_OBJECT_PATH] =
    g_param_spec_string ("object-path", NULL, NULL,
                         "",
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties[PROP_ROLE_ICON_NAME] =
    g_param_spec_string ("role-icon-name", NULL, NULL,
                         "",
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties[PROP_ROLE_NAME] =
    g_param_spec_string ("role-name", NULL, NULL,
                         "",
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
elevado_node_init (ElevadoNode *self)
{
  self->children = g_ptr_array_new_with_free_func (unref_object_nullsafe);
}

ElevadoNode *
elevado_node_new (AtspiAccessible *accessible)
{
  return g_object_new (ELEVADO_TYPE_NODE,
                       "accessible", accessible,
                       NULL);
}

AtspiAccessible *
elevado_node_get_accessible (ElevadoNode *self)
{
  g_return_val_if_fail (ELEVADO_IS_NODE (self), NULL);

  return self->accessible;
}

const char *
elevado_node_get_name (ElevadoNode *self)
{
  g_return_val_if_fail (ELEVADO_IS_NODE (self), NULL);

  return self->name;
}

const char *
elevado_node_get_role_icon_name (ElevadoNode *self)
{
  g_return_val_if_fail (ELEVADO_IS_NODE (self), NULL);

  return icon_name_from_role (self->role);
}

const char *
elevado_node_get_role_name (ElevadoNode *self)
{
  g_return_val_if_fail (ELEVADO_IS_NODE (self), NULL);

  return self->role_name;
}
