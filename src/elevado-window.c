/* elevado-window.c
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#include "config.h"

#include "elevado-window.h"

#include "elevado-autoptr.h"
#include "elevado-node.h"
#include "elevado-node-details.h"
#include "elevado-node-tree.h"
#include "elevado-sidebar-row.h"

#include <atspi/atspi.h>
#include <glib/gi18n.h>

struct _ElevadoWindow
{
  AdwApplicationWindow  parent_instance;

  GtkSortListModel *sort_model;
  ElevadoNodeDetails *details_sidebar;
  AdwOverlaySplitView *split_view;

  AtspiAccessible *desktop;
  AtspiEventListener *event_listener;
};

G_DEFINE_FINAL_TYPE (ElevadoWindow, elevado_window, ADW_TYPE_APPLICATION_WINDOW)

/*
 * Callbacks
 */

static char *
navigation_page_title_from_node_cb (ElevadoWindow *self,
                                    ElevadoNode   *node)
{
  if (node)
    return g_strdup (elevado_node_get_name (node));

  return g_strdup ("");
}

static void
on_sidebar_item_selected (GtkSingleSelection *selection,
                          GParamSpec         *pspec,
                          gpointer            user_data)
{
  ElevadoWindow *self = user_data;
  ElevadoNode *node = gtk_single_selection_get_selected_item (selection);
  
  elevado_node_details_set_node (self->details_sidebar, node);
}

static void
on_node_tree_item_selected (ElevadoNodeTree *tree,
                            ElevadoNode     *node,
                            ElevadoWindow   *self)
{
  elevado_node_details_set_node (self->details_sidebar, node);
  adw_overlay_split_view_set_show_sidebar (self->split_view, node != NULL);
}

static char *
window_title_from_node_cb (ElevadoWindow *self,
                           ElevadoNode   *node)
{
  if (node)
    /* Translators: please do not translate "Elevado", it's the app name */
    return g_strdup_printf (_("Elevado — %s"), elevado_node_get_name (node));

  return g_strdup ("Elevado");
}

static void
elevado_application_about_action (GSimpleAction *action,
                                  GVariant      *parameter,
                                  gpointer       user_data)
{
  ElevadoWindow *self = user_data;
  AdwDialog *about;
  static const char *developers[] = {"Georges Basile Stavracas Neto <georges.stavracas@gmail.com>", NULL};
  static const char *artists[] = {"Brage Fuglseth https://bragefuglseth.dev", NULL};

  g_assert (ELEVADO_IS_WINDOW (self));

  // Create the dialog from metainfo/appdata
  about = adw_about_dialog_new_from_appdata ("/com/feaneron/Elevado/metainfo.xml", PACKAGE_VERSION);

  // Set the proper version, the dialog from appdata sets the one from the appdata/metainfo, not the actual meson one.
  adw_about_dialog_set_version (ADW_ABOUT_DIALOG (about), PACKAGE_VERSION);
  // Set stuff not grabbed from the metainfo.
  adw_about_dialog_set_comments (ADW_ABOUT_DIALOG (about), "Accessibility inspector");
  adw_about_dialog_set_developers (ADW_ABOUT_DIALOG (about), developers);
  adw_about_dialog_set_artists (ADW_ABOUT_DIALOG (about), artists);
  adw_about_dialog_set_copyright (ADW_ABOUT_DIALOG (about), "© 2024 Georges Stavracas");

  // Show the dialog
  adw_dialog_present (ADW_DIALOG (about), GTK_WIDGET (self));
}

static void
elevado_window_class_init (ElevadoWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (ELEVADO_TYPE_NODE);
  g_type_ensure (ELEVADO_TYPE_NODE_TREE);
  g_type_ensure (ELEVADO_TYPE_SIDEBAR_ROW);
  g_type_ensure (ELEVADO_TYPE_NODE_DETAILS);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Elevado/elevado-window.ui");

  gtk_widget_class_bind_template_child (widget_class, ElevadoWindow, details_sidebar);
  gtk_widget_class_bind_template_child (widget_class, ElevadoWindow, sort_model);
  gtk_widget_class_bind_template_child (widget_class, ElevadoWindow, split_view);

  gtk_widget_class_bind_template_callback (widget_class, on_sidebar_item_selected);
  gtk_widget_class_bind_template_callback (widget_class, on_node_tree_item_selected);
  gtk_widget_class_bind_template_callback (widget_class, navigation_page_title_from_node_cb);
  gtk_widget_class_bind_template_callback (widget_class, window_title_from_node_cb);
}

static void
populate_list (ElevadoWindow *self)
{
  int n_children;
  GListStore *roots;

  roots = g_list_store_new (ELEVADO_TYPE_NODE);

  n_children = atspi_accessible_get_child_count (self->desktop, NULL);
  for (int i = 0; i < n_children; i++)
    {
      g_autoptr(AtspiAccessible) child = NULL;
      g_autoptr(ElevadoNode) root = NULL;

      child = atspi_accessible_get_child_at_index (self->desktop, i, NULL);
      root = elevado_node_new (child);

      g_list_store_append (roots, root);
    }

  gtk_sort_list_model_set_model (self->sort_model, G_LIST_MODEL (roots));
}

static void
event_cb (AtspiEvent *event,
          void       *user_data)
{
  ElevadoWindow *self = user_data;

  if (event->source == self->desktop)
    {
      AtspiAccessible *child = g_value_get_object ((const GValue *) &event->any_data);
      AtspiApplication *app = ((AtspiObject *) child)->app;
      GHashTableIter iter;
      char *key;

      /* This is very miserable. The only thing I could find that hints at this being
       * ourselves is that there's a key in the hash that starts without objectpathified
       * app ID. Everything else in child and app is unset at this point.
       */
      g_hash_table_iter_init (&iter, app->hash);
      while (g_hash_table_iter_next (&iter, (gpointer *)&key, NULL))
        {
          if (g_str_has_prefix (key, "/com/feaneron/Elevado"))
            {
              g_debug ("Skipping this app, looks like me (%s)", key);
              return;
            }

          break;
        }

      populate_list (self);
    }
}

static void
elevado_window_init (ElevadoWindow *self)
{
  static const GActionEntry window_actions[] = {
    { "about", elevado_application_about_action },
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   window_actions,
                                   G_N_ELEMENTS (window_actions),
                                   self);

  gtk_widget_init_template (GTK_WIDGET (self));

  if (g_strcmp0 (PROFILE, "development") == 0)
    {
      gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
    }

  self->desktop = atspi_get_desktop (0);
  self->event_listener = atspi_event_listener_new (event_cb, self, NULL);

  atspi_event_listener_register (self->event_listener, "object:children-changed", NULL);

  populate_list (self);
}
