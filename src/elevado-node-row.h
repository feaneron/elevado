/*
 * elevado-node-row.h
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define ELEVADO_TYPE_NODE_ROW (elevado_node_row_get_type())
G_DECLARE_FINAL_TYPE (ElevadoNodeRow, elevado_node_row, ELEVADO, NODE_ROW, AdwBin)

GtkTreeListRow * elevado_node_row_get_row (ElevadoNodeRow *self);
void             elevado_node_row_set_row (ElevadoNodeRow *self,
                                           GtkTreeListRow *row);

void elevado_node_row_collapse (ElevadoNodeRow *self);
void elevado_node_row_expand   (ElevadoNodeRow *self);

G_END_DECLS
