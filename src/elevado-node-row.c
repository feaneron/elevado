/*
 * elevado-node-row.c
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#include "elevado-node-row.h"

#include "elevado-node.h"

#include <glib/gi18n.h>

struct _ElevadoNodeRow
{
  AdwBin parent_instance;

  GtkTreeExpander *expander;

  GtkTreeListRow *row;
};

G_DEFINE_FINAL_TYPE (ElevadoNodeRow, elevado_node_row, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_ROW,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];


/*
 * Callbacks
 */

static char *
accessible_title_from_node_name_cb (ElevadoNodeRow *self,
                                    const char     *name)
{
  if (name && name[0] != '\0')
    return g_strdup (name);

  return g_strdup (_("Unnamed Object"));
}

static GStrv
css_classes_from_node_name_cb (ElevadoNodeRow *self,
                               ElevadoNode    *node)
{
  GStrvBuilder *css_classes = g_strv_builder_new ();
  AtspiAccessible *accessible = elevado_node_get_accessible (node);
  AtspiStateSet *state_set = atspi_accessible_get_state_set (accessible);

  if (!atspi_state_set_contains (state_set, ATSPI_STATE_VISIBLE) &&
      atspi_accessible_get_role (accessible, NULL) != ATSPI_ROLE_APPLICATION)
    g_strv_builder_add (css_classes, "dim-label");

  return g_strv_builder_end (css_classes);
}

/*
 * GObject overrides
 */

static void
elevado_node_row_finalize (GObject *object)
{
  ElevadoNodeRow *self = (ElevadoNodeRow *)object;

  g_clear_object (&self->row);

  G_OBJECT_CLASS (elevado_node_row_parent_class)->finalize (object);
}

static void
elevado_node_row_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ElevadoNodeRow *self = ELEVADO_NODE_ROW (object);

  switch (prop_id)
    {
    case PROP_ROW:
      g_value_set_object (value, self->row);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_row_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ElevadoNodeRow *self = ELEVADO_NODE_ROW (object);

  switch (prop_id)
    {
    case PROP_ROW:
      elevado_node_row_set_row (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_row_class_init (ElevadoNodeRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = elevado_node_row_finalize;
  object_class->get_property = elevado_node_row_get_property;
  object_class->set_property = elevado_node_row_set_property;

  properties[PROP_ROW] =
    g_param_spec_object ("row", NULL, NULL,
                         GTK_TYPE_TREE_LIST_ROW,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

	gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Elevado/elevado-node-row.ui");

	gtk_widget_class_bind_template_child (widget_class, ElevadoNodeRow, expander);

  gtk_widget_class_bind_template_callback (widget_class, accessible_title_from_node_name_cb);
  gtk_widget_class_bind_template_callback (widget_class, css_classes_from_node_name_cb);
}

static void
elevado_node_row_init (ElevadoNodeRow *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));
}

GtkTreeListRow *
elevado_node_row_get_row (ElevadoNodeRow *self)
{
  g_return_val_if_fail (ELEVADO_IS_NODE_ROW (self), NULL);

  return self->row;
}

void
elevado_node_row_set_row (ElevadoNodeRow *self,
                          GtkTreeListRow *row)
{
  g_return_if_fail (ELEVADO_IS_NODE_ROW (self));
  g_return_if_fail (row == NULL || GTK_IS_TREE_LIST_ROW (row));

  if (g_set_object (&self->row, row))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ROW]);
}

void
elevado_node_row_collapse (ElevadoNodeRow *self)
{
  g_return_if_fail (ELEVADO_IS_NODE_ROW (self));

  gtk_widget_activate_action (GTK_WIDGET (self->expander), "listitem.collapse", NULL);
}

void
elevado_node_row_expand (ElevadoNodeRow *self)
{
  g_return_if_fail (ELEVADO_IS_NODE_ROW (self));

  gtk_widget_activate_action (GTK_WIDGET (self->expander), "listitem.expand", NULL);
}
