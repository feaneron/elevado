/*
 * elevado-node-tree.c
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#include "elevado-node-tree.h"

#include "elevado-node.h"
#include "elevado-node-row.h"

struct _ElevadoNodeTree
{
  AdwBin parent_instance;

  GtkColumnView *column_view;
  GtkSingleSelection *single_selection;

  ElevadoNode *root;
};

G_DEFINE_FINAL_TYPE (ElevadoNodeTree, elevado_node_tree, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_ROOT,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];

enum
{
  SELECTED,

  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

/*
 * Callbacks
 */

static GListModel *
create_model_func (gpointer item,
                   gpointer user_data)
{
  if (g_list_model_get_n_items (item) == 0)
    return NULL;

  return g_object_ref (item);
}

static gboolean
tree_expander_shortcut_cb (GtkWidget *widget,
                           GVariant  *args,
                           gpointer   user_data)
{
  ElevadoNodeTree *self = ELEVADO_NODE_TREE (widget);
  GtkTextDirection direction;
  GtkTreeListRow *row;
  GtkWidget *child;
  gboolean collapse;
  guint keyval = GPOINTER_TO_INT (user_data);

  direction = gtk_widget_get_direction (widget);

  /* Hack to find the focus item. */
  row = NULL;
  child = gtk_root_get_focus (gtk_widget_get_root (widget));

  while (child && !ELEVADO_IS_NODE_ROW (child))
    child = gtk_widget_get_first_child (child);

  if (!child)
    return FALSE;

  g_assert (child != NULL && ELEVADO_IS_NODE_ROW (child));

  row = elevado_node_row_get_row (ELEVADO_NODE_ROW (child));
  g_assert (row != NULL);
  /* End of hack. */

  if ((keyval == GDK_KEY_Right && direction == GTK_TEXT_DIR_LTR) ||
      (keyval == GDK_KEY_Left && direction == GTK_TEXT_DIR_RTL))
    {
      collapse = FALSE;
    }
  else
    {
      collapse = TRUE;
    }

  if (!gtk_tree_list_row_get_expanded (row) && collapse)
    {
      g_autoptr(GtkTreeListRow) parent = gtk_tree_list_row_get_parent (row);

      if (parent != NULL)
        {
          guint parent_position = gtk_tree_list_row_get_position (parent);

          g_assert (parent_position != GTK_INVALID_LIST_POSITION);

          gtk_column_view_scroll_to (self->column_view,
                                     parent_position,
                                     NULL,
                                     GTK_LIST_SCROLL_FOCUS | GTK_LIST_SCROLL_SELECT,
                                     NULL);
        }
    }
  else
    {
      if (collapse)
        elevado_node_row_collapse (ELEVADO_NODE_ROW (child));
      else
        elevado_node_row_expand (ELEVADO_NODE_ROW (child));
    }

  return TRUE;
}

static void
on_column_view_activate (GtkColumnView *column_view,
                         unsigned int   position,
                         gpointer       user_data)
{
  ElevadoNodeTree *self = user_data;
  GtkSelectionModel *model = gtk_column_view_get_model (column_view);
  GtkTreeListRow *row = g_list_model_get_item (G_LIST_MODEL (model), position);
  ElevadoNode *node = gtk_tree_list_row_get_item (row);

  g_signal_emit (self, signals[SELECTED], 0, node);
}

/*
 * GObject overrides
 */

static void
elevado_node_tree_finalize (GObject *object)
{
  ElevadoNodeTree *self = (ElevadoNodeTree *)object;

  g_clear_object (&self->root);

  G_OBJECT_CLASS (elevado_node_tree_parent_class)->finalize (object);
}

static void
elevado_node_tree_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  ElevadoNodeTree *self = ELEVADO_NODE_TREE (object);

  switch (prop_id)
    {
    case PROP_ROOT:
      g_value_set_object (value, self->root);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_tree_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  ElevadoNodeTree *self = ELEVADO_NODE_TREE (object);

  switch (prop_id)
    {
    case PROP_ROOT:
      elevado_node_tree_set_root (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_tree_class_init (ElevadoNodeTreeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (ELEVADO_TYPE_NODE_ROW);

  object_class->finalize = elevado_node_tree_finalize;
  object_class->get_property = elevado_node_tree_get_property;
  object_class->set_property = elevado_node_tree_set_property;

  properties[PROP_ROOT] =
    g_param_spec_object ("root", NULL, NULL,
                         ELEVADO_TYPE_NODE,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
  /**
   * ElevadoNodeTree::selected
   * @node: the selected node
   */
  signals[SELECTED] =
    g_signal_new ("selected",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1,
                  ELEVADO_TYPE_NODE);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Elevado/elevado-node-tree.ui");

  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeTree, column_view);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeTree, single_selection);
  gtk_widget_class_bind_template_callback (widget_class, on_column_view_activate);
}

static void
elevado_node_tree_init (ElevadoNodeTree *self)
{
  GtkEventController *controller;
  GtkShortcut *shortcut;

  gtk_widget_init_template (GTK_WIDGET (self));

  controller = gtk_shortcut_controller_new ();
  gtk_event_controller_set_propagation_phase (controller, GTK_PHASE_CAPTURE);
  gtk_widget_add_controller (GTK_WIDGET (self), controller);

  shortcut = gtk_shortcut_new (gtk_keyval_trigger_new (GDK_KEY_Left, 0),
                               gtk_callback_action_new (tree_expander_shortcut_cb,
                                                        GINT_TO_POINTER (GDK_KEY_Left),
                                                        NULL));
  gtk_shortcut_controller_add_shortcut (GTK_SHORTCUT_CONTROLLER (controller), shortcut);

  shortcut = gtk_shortcut_new (gtk_keyval_trigger_new (GDK_KEY_Right, 0),
                               gtk_callback_action_new (tree_expander_shortcut_cb,
                                                        GINT_TO_POINTER (GDK_KEY_Right),
                                                        NULL));
  gtk_shortcut_controller_add_shortcut (GTK_SHORTCUT_CONTROLLER (controller), shortcut);
}

ElevadoNode *
elevado_node_tree_get_root (ElevadoNodeTree *self)
{
  g_return_val_if_fail (ELEVADO_IS_NODE_TREE (self), NULL);

  return self->root;
}

void
elevado_node_tree_set_root (ElevadoNodeTree *self,
                            ElevadoNode     *root)
{
  g_return_if_fail (ELEVADO_IS_NODE_TREE (self));

  if (self->root != root)
    {
      if (self->root)
        gtk_single_selection_set_model (self->single_selection, NULL);

      g_set_object (&self->root, root);

      if (root)
        {
          g_autoptr(GtkTreeListModel) model = NULL;
          GListStore *store;

          store = g_list_store_new (ELEVADO_TYPE_NODE);
          g_list_store_append (store, root);

          model = gtk_tree_list_model_new (G_LIST_MODEL (store),
                                           FALSE, FALSE,
                                           create_model_func,
                                           self,
                                           NULL);

          gtk_single_selection_set_model (self->single_selection, G_LIST_MODEL (model));
        }

      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ROOT]);
    }
}
