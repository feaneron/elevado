/*
 * elevado-node-details.c
 *
 * Copyright 2024 Red Hat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Bilal Elmoussaoui <belmouss@redhat.com>
 */

#include "elevado-node-details.h"
#include "elevado-autoptr.h"

#include <glib/gi18n.h>

struct _ElevadoNodeDetails
{
  AdwBin parent_instance;
  
  AdwActionRow *name_row;
  AdwActionRow *description_row;
  AdwActionRow *state_row;
  AdwActionRow *interfaces_row;
  AdwActionRow *help_text_row;
  AdwActionRow *index_row;
  AdwActionRow *role_row;

  AdwPreferencesGroup *component_group;
  AdwActionRow *component_size_row;
  AdwActionRow *component_position_row;
  AdwActionRow *component_layer_row;
  AdwActionRow *component_alpha_row;
  AdwActionRow *component_mdi_z_order_row;

  AdwPreferencesGroup *toolkit_group;
  AdwActionRow *toolkit_name_row;
  AdwActionRow *toolkit_version_row;
  AdwActionRow *toolkit_atspi_version_row;
  AdwActionRow *toolkit_application_id_row;
  AdwActionRow *toolkit_process_id_row;

  AdwPreferencesGroup *value_group;
  AdwActionRow *current_value_row;
  AdwActionRow *minimum_value_row;
  AdwActionRow *maximum_value_row;
  AdwActionRow *minimum_increment_value_row;
  AdwActionRow *text_value_row;

  AdwPreferencesGroup *text_group;
  AdwActionRow *text_row;
  AdwActionRow *caret_row;

  AdwPreferencesGroup *action_group;
  GPtrArray *actions;

  AdwPreferencesGroup *relations_group;
  GPtrArray *relations;

  ElevadoNode *node;

  AtspiEventListener *event_listener;
};

static void set_row_subtitle (AdwActionRow *row,
                              const char   *format,
                              ...) G_GNUC_PRINTF (2, 3);

G_DEFINE_FINAL_TYPE (ElevadoNodeDetails, elevado_node_details, ADW_TYPE_BIN)

enum
{
  PROP_0,
  PROP_NODE,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];

/*
 * Auxiliary methods
 */

static void
set_row_subtitle (AdwActionRow *row,
                  const char   *format,
                  ...)
{
  g_autofree char *formatted_subtitle = NULL;
  va_list args;

  va_start (args, format);
  formatted_subtitle = g_strdup_vprintf (format, args);
  va_end (args);

  adw_action_row_set_subtitle (row, formatted_subtitle);
}

static void
elevado_node_details_update_component_group (ElevadoNodeDetails  *self,
                                             AtspiAccessible     *accessible)
{
  g_autoptr (AtspiComponent) component = atspi_accessible_get_component_iface (accessible);
  AtspiPoint *size, *position;

  if (!component)
    {
      gtk_widget_set_visible (GTK_WIDGET (self->component_group), FALSE);
      return;
    }

  position = atspi_component_get_position (component, ATSPI_COORD_TYPE_WINDOW, NULL);
  size = atspi_component_get_size (component, NULL);

  set_row_subtitle (self->component_alpha_row, "%f", atspi_component_get_alpha (component, NULL));
  set_row_subtitle (self->component_mdi_z_order_row, "%d", atspi_component_get_mdi_z_order (component, NULL));
  set_row_subtitle (self->component_size_row, "(%d, %d)", size->x, size->y);
  set_row_subtitle (self->component_position_row, "(%d, %d)", position->x, position->y);

  switch (atspi_component_get_layer (component, NULL))
    {
      case ATSPI_LAYER_BACKGROUND:
        adw_action_row_set_subtitle (self->component_layer_row, _("Background"));
        break;
      case ATSPI_LAYER_CANVAS:
        adw_action_row_set_subtitle (self->component_layer_row, _("Canvas"));
        break;
      case ATSPI_LAYER_WIDGET:
        adw_action_row_set_subtitle (self->component_layer_row, _("Widget"));
        break;
      case ATSPI_LAYER_MDI:
        adw_action_row_set_subtitle (self->component_layer_row, _("MDI"));
        break;
      case ATSPI_LAYER_POPUP:
        adw_action_row_set_subtitle (self->component_layer_row, _("Popup"));
        break;
      case ATSPI_LAYER_OVERLAY:
        adw_action_row_set_subtitle (self->component_layer_row, _("Overlay"));
        break;
      case ATSPI_LAYER_WINDOW:
        adw_action_row_set_subtitle (self->component_layer_row, _("Window"));
        break;

      case ATSPI_LAYER_LAST_DEFINED:
      case ATSPI_LAYER_INVALID:
      default:
        adw_action_row_set_subtitle (self->component_layer_row, _("Invalid"));
        break;
    }

  gtk_widget_set_visible (GTK_WIDGET (self->component_group), TRUE);
}

static void
elevado_node_details_update_value_group (ElevadoNodeDetails  *self,
                                         AtspiAccessible     *accessible)
{
  g_autoptr (AtspiValue) value = atspi_accessible_get_value_iface (accessible);
  g_autofree char *value_text = NULL;

  if (!value)
    {
      gtk_widget_set_visible (GTK_WIDGET (self->value_group), FALSE);
      return;
    }

  value_text = atspi_value_get_text (value, NULL);
  
  set_row_subtitle (self->current_value_row, "%f", atspi_value_get_current_value (value, NULL));
  set_row_subtitle (self->minimum_value_row, "%f", atspi_value_get_minimum_value (value, NULL));
  set_row_subtitle (self->maximum_value_row, "%f", atspi_value_get_maximum_value (value, NULL));
  set_row_subtitle (self->minimum_increment_value_row, "%f", atspi_value_get_minimum_increment (value, NULL));
  set_row_subtitle (self->text_value_row, "%s", value_text);

  gtk_widget_set_visible (GTK_WIDGET (self->value_group), TRUE);
}

static void
elevado_node_details_update_text_group (ElevadoNodeDetails  *self,
                                        AtspiAccessible     *accessible)
{
  g_autoptr (AtspiText) text = atspi_accessible_get_text_iface (accessible);
  g_autofree char *text_value = NULL;
  int length;
  int caret_offset;

  if (!text)
    {
      gtk_widget_set_visible (GTK_WIDGET (self->text_group), FALSE);
      return;
    }

  length = atspi_text_get_character_count (text, NULL);
  text_value = atspi_text_get_text (text, 0, length, NULL);
  caret_offset = atspi_text_get_caret_offset (text, NULL);

  set_row_subtitle (self->text_row, "%s", text_value);
  set_row_subtitle (self->caret_row, "%d", caret_offset);

  gtk_widget_set_visible (GTK_WIDGET (self->text_group), TRUE);
}

static void
elevado_node_details_update_action_group (ElevadoNodeDetails  *self,
                                          AtspiAccessible     *accessible)
{
  g_autoptr (AtspiAction) action = atspi_accessible_get_action_iface (accessible);
  int n_actions;

  if (!action || atspi_action_get_n_actions (action, NULL) == 0)
    {
      gtk_widget_set_visible (GTK_WIDGET (self->action_group), FALSE);
      return;
    }


  for (int i = 0; i < self->actions->len; i++)
    adw_preferences_group_remove (self->action_group, GTK_WIDGET (g_ptr_array_index (self->actions, i)));
  g_ptr_array_set_size (self->actions, 0);

  n_actions = atspi_action_get_n_actions (action, NULL);

  for (int i = 0; i < n_actions; i++)
    {
      g_autofree char *name = atspi_action_get_action_name (action, i, NULL);
      g_autofree char *desc = atspi_action_get_action_description (action, i, NULL);
      g_autofree char *keys = atspi_action_get_key_binding (action, i, NULL);
      GtkWidget *row;

      row = adw_action_row_new ();

      adw_preferences_row_set_title (ADW_PREFERENCES_ROW (row), name);
      adw_action_row_set_subtitle (ADW_ACTION_ROW (row), desc);

      if (g_strcmp0 (keys, ";;") != 0)
        adw_action_row_add_suffix (ADW_ACTION_ROW (row), gtk_label_new (keys));

      adw_preferences_group_add (self->action_group, row);
      g_ptr_array_add (self->actions, row);
    }

  gtk_widget_set_visible (GTK_WIDGET (self->action_group), TRUE);
}

static const char *
get_relation_name (AtspiRelationType type)
{
  const char *names[] = {
    "",
    N_("Label For"),
    N_("Labelled By"),
    N_("Controller For"),
    N_("Controlled By"),
    N_("Member Of"),
    N_("Tooltip For"),
    N_("Node Child Of"),
    N_("Node Parent Of"),
    N_("Extended"),
    N_("Flows To"),
    N_("Flows From"),
    N_("Subwindow Of"),
    N_("Embeds"),
    N_("Embedded By"),
    N_("Popup For"),
    N_("Parent Window Of"),
    N_("Description For"),
    N_("Described By"),
    N_("Details"),
    N_("Details For"),
    N_("Error Message"),
    N_("Error For"),
    "",
  };

  return _(names[type]);
}

static void
elevado_node_details_update_relations_group (ElevadoNodeDetails  *self,
                                             AtspiAccessible     *accessible)
{
  g_autoptr (GArray) relations = atspi_accessible_get_relation_set (accessible, NULL);

  if (!relations || relations->len == 0)
    {
      gtk_widget_set_visible (GTK_WIDGET (self->relations_group), FALSE);
      return;
    }


  for (int i = 0; i < self->relations->len; i++)
    adw_preferences_group_remove (self->relations_group, GTK_WIDGET (g_ptr_array_index (self->relations, i)));
  g_ptr_array_set_size (self->relations, 0);

  for (int i = 0; i < relations->len; i++)
    {
      AtspiRelation *relation = g_array_index (relations, AtspiRelation *, i);
      GtkWidget *row;
      int n_targets;
      AtspiRelationType type;

      row = adw_action_row_new ();

      type = atspi_relation_get_relation_type (relation);
      n_targets = atspi_relation_get_n_targets (relation);

      adw_preferences_row_set_title (ADW_PREFERENCES_ROW (row), get_relation_name (type));
      set_row_subtitle (ADW_ACTION_ROW (row), ngettext ("%d Object", "%d Objects", n_targets), n_targets);

      for (int j = 0; j < n_targets; j++)
        {
          g_autoptr (AtspiAccessible) target = atspi_relation_get_target (relation, j);
          g_autofree char *target_name = atspi_accessible_get_name (target, NULL);
          GtkWidget *button = gtk_button_new_with_label (target_name && *target_name ? target_name : _("Unnamed Object"));

          gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
          gtk_button_set_has_frame (GTK_BUTTON (button), FALSE);

          adw_action_row_add_suffix (ADW_ACTION_ROW (row), button);
          if (j > 0)
            gtk_orientable_set_orientation (GTK_ORIENTABLE (gtk_widget_get_parent (button)), GTK_ORIENTATION_VERTICAL);
        }


      adw_preferences_group_add (self->relations_group, row);
      g_ptr_array_add (self->relations, row);
    }

  gtk_widget_set_visible (GTK_WIDGET (self->relations_group), TRUE);
}

static char *
accessible_states_as_string (AtspiStateSet *states_set)
{
  g_autoptr (GPtrArray) ptr_array = g_ptr_array_new ();

  struct {
    guint flag;
    const char *name;
  } flags[] = {
    { ATSPI_STATE_INVALID, N_("Invalid") },
    { ATSPI_STATE_ACTIVE, N_("Active") },
    { ATSPI_STATE_ARMED, N_("Armed") },
    { ATSPI_STATE_BUSY, N_("Busy") },
    { ATSPI_STATE_CHECKED, N_("Checked") },
    { ATSPI_STATE_COLLAPSED, N_("Collapsed") },
    { ATSPI_STATE_DEFUNCT, N_("Defunct") },
    { ATSPI_STATE_EDITABLE, N_("Editable") },
    { ATSPI_STATE_ENABLED, N_("Enabled") },
    { ATSPI_STATE_EXPANDABLE, N_("Expandable") },
    { ATSPI_STATE_EXPANDED, N_("Expanded") },
    { ATSPI_STATE_FOCUSABLE, N_("Focusable") },
    { ATSPI_STATE_FOCUSED, N_("Focused") },
    { ATSPI_STATE_HAS_TOOLTIP, N_("Has Tooltip") },
    { ATSPI_STATE_HORIZONTAL, N_("Horizontal") },
    { ATSPI_STATE_ICONIFIED, N_("Iconified") },
    { ATSPI_STATE_MODAL, N_("Modal") },
    { ATSPI_STATE_MULTI_LINE, N_("Multi Line") },
    { ATSPI_STATE_MULTISELECTABLE, N_("Multi Selectable") },
    { ATSPI_STATE_OPAQUE, N_("Opaque") },
    { ATSPI_STATE_PRESSED, N_("Pressed") },
    { ATSPI_STATE_RESIZABLE, N_("Resizable") },
    { ATSPI_STATE_SELECTABLE, N_("Selectable") },
    { ATSPI_STATE_SELECTED, N_("Selected") },
    { ATSPI_STATE_SENSITIVE, N_("Sensitive") },
    { ATSPI_STATE_SHOWING, N_("Showing") },
    { ATSPI_STATE_SINGLE_LINE, N_("Single Line") },
    { ATSPI_STATE_STALE, N_("Stale") },
    { ATSPI_STATE_TRANSIENT, N_("Transient") },
    { ATSPI_STATE_VERTICAL, N_("Vertical") },
    { ATSPI_STATE_VISIBLE, N_("Visible") },
    { ATSPI_STATE_MANAGES_DESCENDANTS, N_("Manages Descendants") },
    { ATSPI_STATE_INDETERMINATE, N_("Indeterminate") },
    { ATSPI_STATE_REQUIRED, N_("Required") },
    { ATSPI_STATE_TRUNCATED, N_("Truncated") },
    { ATSPI_STATE_ANIMATED, N_("Animated") },
    { ATSPI_STATE_INVALID_ENTRY, N_("Invalid Entry") },
    { ATSPI_STATE_SUPPORTS_AUTOCOMPLETION, N_("Supports Autocompletion") },
    { ATSPI_STATE_SELECTABLE_TEXT, N_("Selectable Text") },
    { ATSPI_STATE_IS_DEFAULT, N_("Default") },
    { ATSPI_STATE_VISITED, N_("Visited") },
    { ATSPI_STATE_CHECKABLE, N_("Checkable") },
    { ATSPI_STATE_HAS_POPUP, N_("Has Popup") },
    { ATSPI_STATE_READ_ONLY, N_("Read Only") },
  };

  for (size_t i = 0; i < G_N_ELEMENTS (flags); i++)
    {
      if (atspi_state_set_contains (states_set, flags[i].flag))
        g_ptr_array_add (ptr_array, (gpointer) gettext (flags[i].name));
    }
  g_ptr_array_add (ptr_array, NULL);

  return g_strjoinv (", ", (char **)ptr_array->pdata);
}

static void
elevado_node_details_update_toolkit_group (ElevadoNodeDetails  *self,
                                           AtspiAccessible     *accessible)
{
  AtspiRole role = atspi_accessible_get_role (accessible, NULL);
  g_autofree char *toolkit_name = NULL;
  g_autofree char *toolkit_version = NULL;
  g_autofree char *atspi_version = NULL;

  if (role != ATSPI_ROLE_APPLICATION)
    {
      gtk_widget_set_visible (GTK_WIDGET (self->toolkit_group), FALSE);
      return;
    }

  toolkit_name = atspi_accessible_get_toolkit_name (accessible, NULL);
  adw_action_row_set_subtitle (self->toolkit_name_row, toolkit_name);

  toolkit_version = atspi_accessible_get_toolkit_version (accessible, NULL);
  adw_action_row_set_subtitle (self->toolkit_version_row, toolkit_version);

  atspi_version = atspi_accessible_get_atspi_version (accessible, NULL);
  adw_action_row_set_subtitle (self->toolkit_atspi_version_row, atspi_version);

  set_row_subtitle (self->toolkit_application_id_row, "%d", atspi_accessible_get_id (accessible, NULL));
  set_row_subtitle (self->toolkit_process_id_row, "%d", atspi_accessible_get_process_id (accessible, NULL));

  gtk_widget_set_visible (GTK_WIDGET (self->toolkit_group), TRUE);
}

static inline gboolean
is_string_empty (const char *text)
{
  return text == NULL || *text == '\0';
}


/*
 * GObject overrides
 */

static void
elevado_node_details_finalize (GObject *object)
{
  ElevadoNodeDetails *self = (ElevadoNodeDetails *)object;

  g_clear_pointer (&self->relations, g_ptr_array_unref);
  g_clear_pointer (&self->actions, g_ptr_array_unref);
  g_clear_object (&self->node);

  atspi_event_listener_deregister (self->event_listener, "object:", NULL);
  g_clear_object (&self->event_listener);

  G_OBJECT_CLASS (elevado_node_details_parent_class)->finalize (object);
}

static void
elevado_node_details_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  ElevadoNodeDetails *self = ELEVADO_NODE_DETAILS (object);

  switch (prop_id)
    {
    case PROP_NODE:
      g_value_set_object (value, self->node);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_details_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  ElevadoNodeDetails *self = ELEVADO_NODE_DETAILS (object);

  switch (prop_id)
    {
    case PROP_NODE:
      elevado_node_details_set_node (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_node_details_class_init (ElevadoNodeDetailsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = elevado_node_details_finalize;
  object_class->get_property = elevado_node_details_get_property;
  object_class->set_property = elevado_node_details_set_property;

  properties[PROP_NODE] =
    g_param_spec_object ("node", NULL, NULL,
                         ELEVADO_TYPE_NODE,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Elevado/elevado-node-details.ui");

  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, name_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, description_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, state_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, interfaces_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, help_text_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, index_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, role_row);

  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, toolkit_group);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, toolkit_name_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, toolkit_version_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, toolkit_atspi_version_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, toolkit_application_id_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, toolkit_process_id_row);

  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, component_group);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, component_size_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, component_position_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, component_layer_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, component_alpha_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, component_mdi_z_order_row);

  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, value_group);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, current_value_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, minimum_value_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, maximum_value_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, minimum_increment_value_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, text_value_row);

  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, text_group);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, text_row);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, caret_row);

  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, action_group);
  gtk_widget_class_bind_template_child (widget_class, ElevadoNodeDetails, relations_group);
}

static void
update_details (ElevadoNodeDetails *self)
{
  AtspiAccessible *accessible = elevado_node_get_accessible (self->node);
  AtspiStateSet *state_set;
  GArray *interfaces;
  g_autofree char *node_description = NULL;
  g_autofree char *node_help_text = NULL;
  const char *node_name;
  g_autofree char *node_states = NULL;
  g_autofree char *node_interfaces = NULL;

  state_set = atspi_accessible_get_state_set (accessible);
  node_name = elevado_node_get_name (self->node);
  adw_action_row_set_subtitle (self->name_row, node_name);
  gtk_widget_set_visible (GTK_WIDGET (self->name_row), !is_string_empty (node_name));

  node_description = atspi_accessible_get_description (accessible, NULL);
  adw_action_row_set_subtitle (self->description_row, node_description);
  gtk_widget_set_visible (GTK_WIDGET (self->description_row), !is_string_empty (node_description));

  set_row_subtitle (self->role_row, "%s", elevado_node_get_role_name (self->node));
  set_row_subtitle (self->index_row, "%d", atspi_accessible_get_index_in_parent (accessible, NULL));

  node_states = accessible_states_as_string (state_set);
  adw_action_row_set_subtitle (self->state_row, node_states);
  gtk_widget_set_visible (GTK_WIDGET (self->state_row), !is_string_empty (node_states));

  node_help_text = atspi_accessible_get_help_text (accessible, NULL);
  adw_action_row_set_subtitle (self->help_text_row, node_help_text);
  gtk_widget_set_visible (GTK_WIDGET (self->help_text_row), !is_string_empty (node_help_text));

  interfaces = atspi_accessible_get_interfaces (accessible);
  node_interfaces = g_strjoinv (", ", (char **) g_array_steal (interfaces, NULL));
  adw_action_row_set_subtitle (self->interfaces_row, node_interfaces);

  elevado_node_details_update_component_group (self, accessible);
  elevado_node_details_update_value_group (self, accessible);
  elevado_node_details_update_text_group (self, accessible);
  elevado_node_details_update_action_group (self, accessible);
  elevado_node_details_update_relations_group (self, accessible);
  elevado_node_details_update_toolkit_group (self, accessible);
}

static void
event_cb (AtspiEvent *event,
          void       *data)
{
  ElevadoNodeDetails *self = data;

  if (!self->node)
    return;

  if (event->source == elevado_node_get_accessible (self->node))
    update_details (self);
}

static void
elevado_node_details_init (ElevadoNodeDetails *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->actions = g_ptr_array_new ();
  self->relations = g_ptr_array_new ();

  self->event_listener = atspi_event_listener_new (event_cb, self, NULL);
  atspi_event_listener_register (self->event_listener, "object:", NULL);
}

void
elevado_node_details_set_node (ElevadoNodeDetails *self,
                               ElevadoNode        *node)
{
  g_return_if_fail (ELEVADO_IS_NODE_DETAILS (self));
  g_return_if_fail (node == NULL || ELEVADO_IS_NODE (node));

  if (g_set_object (&self->node, node))
    {
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NODE]);

      if (node)
        update_details (self);
    }
}

