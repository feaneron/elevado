/*
 * elevado-sidebar-row.c
 *
 * Copyright 2024 GNOME Foundation Inc.
 *           2024 Igalia S.L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Authors:
 *   Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 */

#include "elevado-sidebar-row.h"

#include "elevado-node.h"

struct _ElevadoSidebarRow
{
  AdwBin parent_instance;

  GtkLabel *name_label;

  ElevadoNode *node;
};

G_DEFINE_FINAL_TYPE (ElevadoSidebarRow, elevado_sidebar_row, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_NODE,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];

/*
 * Auxiliary functions
 */

static void
update_row_for_node (ElevadoSidebarRow *self)
{
  const char *name = NULL;

  if (!self->node)
    return;

  if ((name = elevado_node_get_name (self->node)))
    gtk_label_set_label (self->name_label, name);
  else
    gtk_label_set_label (self->name_label, "Unnamed");
}

/*
 * GObject overrides
 */

static void
elevado_sidebar_row_finalize (GObject *object)
{
  ElevadoSidebarRow *self = (ElevadoSidebarRow *)object;

  g_clear_object (&self->node);

  G_OBJECT_CLASS (elevado_sidebar_row_parent_class)->finalize (object);
}

static void
elevado_sidebar_row_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  ElevadoSidebarRow *self = ELEVADO_SIDEBAR_ROW (object);

  switch (prop_id)
    {
    case PROP_NODE:
      g_value_set_object (value, self->node);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_sidebar_row_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  ElevadoSidebarRow *self = ELEVADO_SIDEBAR_ROW (object);

  switch (prop_id)
    {
    case PROP_NODE:
      elevado_sidebar_row_set_node (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elevado_sidebar_row_class_init (ElevadoSidebarRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = elevado_sidebar_row_finalize;
  object_class->get_property = elevado_sidebar_row_get_property;
  object_class->set_property = elevado_sidebar_row_set_property;

  properties[PROP_NODE] =
    g_param_spec_object ("node", NULL, NULL,
                         ELEVADO_TYPE_NODE,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

	gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Elevado/elevado-sidebar-row.ui");

	gtk_widget_class_bind_template_child (widget_class, ElevadoSidebarRow, name_label);
}

static void
elevado_sidebar_row_init (ElevadoSidebarRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

ElevadoNode *
elevado_sidebar_row_get_node (ElevadoSidebarRow *self)
{
  g_return_val_if_fail (ELEVADO_IS_SIDEBAR_ROW (self), NULL);

  return self->node;
}

void
elevado_sidebar_row_set_node (ElevadoSidebarRow *self,
                              ElevadoNode       *node)
{
  g_return_if_fail (ELEVADO_IS_SIDEBAR_ROW (self));

  if (g_set_object (&self->node, node))
    {
      update_row_for_node (self);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NODE]);
    }
}
