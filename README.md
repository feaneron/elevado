# Elevado

Accessibility inspector.

Elevado allows browsing through the accessibility objects that apps set for
themselves. The primary goal is to make it easier to spot where apps fall apart
in terms of accessibility.

## Sandboxing

Due to constraints in the accessibility stack, Elevado cannot run as a sandboxed
app.

## Building

```
$ meson setup . _build
$ ninja -C _build
$ _build/src/elevado
```
